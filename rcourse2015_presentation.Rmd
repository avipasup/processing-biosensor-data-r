---
title: "Detection of Tetracyclines in Animal Meat using ptetlux1 Bioreporter"
output: slidy_presentation
---


- Bioluminescent bioreporter is used to measure residual antibiotics in meat samples
- Sample concentrations were pre determined using HPLC-Mass spectrometry at EVIRA 
- Lights On and Lights Off (control) strains are used to make luminesence measurements.

## Samples
```{r, echo=FALSE}
samples.data <- read.csv(file="avinash_data/samples.csv")
knitr::kable(samples.data, caption = "Samples used in this Study")
```

## Analysis
 - Luminesence values were measured from meat samples and tetracycline standards.
 - Calculations were done to find fold inductions (used in standard plot)
 - Equation from the standard plot is used to determine tet concentrations  from samples
 
## Results
```{r, echo=FALSE}
lumstd1on.data <- read.csv(file="avinash_data/Experiment 1/avinash_stdon_expt1.csv")
knitr::kable(lumstd1on.data [2:6], digits = 2, caption = "Luminescence values from measuring standard tetracycline solutions with Lights On strain")
```

## Calculations


```{r, echo=FALSE}
knitr::kable(lumvalsstd1on.data[2:12], digits = 2, caption = "Data Analysis of standards from lights on measurement")
```

##

```{r, echo=FALSE}
knitr::kable(lumvalsstd1off.data[2:10], digits = 2, caption = "Data Analysis of standards from lights off measurement")
```

##
```{r, echo=FALSE}
knitr::kable(lumvalssamp1on.data[1:11], digits = 2, caption = "Data Analysis of samples from lights on measurement ")
```


##
The CF values calculated from the lights off strain were used to correct the fold induction values for sample toxicity.


```{r, echo=FALSE}
knitr::kable(lumvalssamp1off.data[1:10], digits = 2, caption = "Data Analysis of samples from lights off measurement")
```

##

![Luminescence vs RLU from using Lights on reporter](./plots/Experiment 1/concvsrlu1on.png)

##

![Luminescence vs RLU from using Lights off reporter](./plots/Experiment 1/concvsrlu1off.png)

##

![Luminescence from sample extracts measured using Lights on reporter](./plots/Experiment 1/extsampvsrlu1on.png)

##

![Luminescence from sample extracts measured using Lights off reporter](./plots/Experiment 1/extsampvsrlu1off.png)

##
![Luminescence vs FIc from using Lights off reporter](./plots/Experiment 1/concvsfic1on.png)

##Solid Meat Samples

![Luminescence vs RLU from using Lights ON reporter from Solid Meat ](./plots/Experiment 1/solidvsrlu1on.png)

##

![Luminescence vs RLU from using Lights OFF reporter from Solid Meat ](./plots/Experiment 1/solidvsrlu1off.png)

##

```{r, echo =FALSE}
t.test(lumvalsstd1on.data$average_rlu, lumvalsstd2on.data$average_rlu)
t.test(lumvalssamp1on.data$average_rlu, lumvalssamp2on.data$average_rlu)
```
